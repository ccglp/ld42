﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMovement : MonoBehaviour {
    private Vector3 center;
    private float radiusX, radiusY;
    private float degrees = 0;

    private float rotation = 0; 
    [SerializeField]
    private float degreesPerSecond = 60;
    [SerializeField][Range(0.3f, 2)]
    private float weightRadiusX = 1, weightRadiusY = 1;

    [SerializeField]
    private float rotationSpeedPerSecond = 200;

    [SerializeField]
    private AnimationCurve aumentYByX; 

	// Use this for initialization
	void Start () {
        center = GameObject.FindGameObjectWithTag("Center").transform.position;
        radiusX = Vector3.Distance(this.transform.position, center);
       
        radiusY = radiusX;

        radiusX *= weightRadiusX;
        radiusY *= weightRadiusY; 

        degrees = Random.Range(0, 360); 
    }

    // Update is called once per frame
    void Update () {

        Rotation();
        Translation(); 
       
	}

    private void Translation()
    {
        degrees += degreesPerSecond * Time.deltaTime;

        if (degrees >= 360)
        {
            degrees = 0;
        }

        float radians = degrees * Mathf.Deg2Rad;



        this.transform.position = new Vector3(Mathf.Cos(radians) * radiusX, Mathf.Sin(radians) * radiusY , 0);
        this.transform.position += Vector3.up * aumentYByX.Evaluate(this.transform.position.x /radiusX);
    }
    private void Rotation()
    {
        rotation += rotationSpeedPerSecond * Time.deltaTime;

        this.transform.eulerAngles = new Vector3(0, rotation, 0);

        if (rotation >= 360)
        {
            rotation = 0;
        }
    }
}
