﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunAutoRotate : MonoBehaviour {

    [SerializeField]
    private float rotateSpeed = 300;
    private float rotation = 0; 

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        rotation += rotateSpeed * Time.deltaTime; 
        if (rotation >= 360)
        {
            rotation = 0; 
        }
        this.transform.eulerAngles = new Vector3(0, rotation, 0); 

	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.CompareTag("Bad"))
		{
			Destroy(collision.collider.gameObject);
		}
	}
}
