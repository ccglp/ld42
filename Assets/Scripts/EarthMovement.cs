﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthMovement : MonoBehaviour {
    private Rigidbody rb;
    private Vector3 center;

    [SerializeField]
    private float force = 20, sunForce = 10; 
	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody>();
        center = GameObject.FindGameObjectWithTag("Center").transform.position;
        rb.velocity = new Vector3(0, 4, 0); 
	}
	
	// Update is called once per frame
	void Update () {
        rb.AddForce(new Vector3(Input.GetAxis("Horizontal")*force, Input.GetAxis("Vertical")*force, 0));

        rb.AddForce((center - this.transform.position) * sunForce); 
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Bad") || collision.collider.CompareTag("Center")|| collision.collider.CompareTag("Collapser"))
        {
            GameController.Instance.Lose(); 
        }
    }
}
