﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitLines : MonoBehaviour {
    [SerializeField]
    private int frameOffset = 10;
    [SerializeField]
    private int maxNumberOfPoints = 100;
    [SerializeField]
    private Color lineColor = Color.white; 

    private LineRenderer line;
    private List<Vector3> positions; 
	// Use this for initialization
	void Start () {
        line = this.GetComponent<LineRenderer>();
        line.startColor = lineColor;
        line.endColor = lineColor;
        line.useWorldSpace = true;
        positions = new List<Vector3>(); 
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.frameCount % frameOffset == 0)
        {
            line.positionCount = positions.Count + 1; 
            line.SetPosition(line.positionCount-1, this.transform.position);
            positions.Add(this.transform.position); 
            if (positions.Count > maxNumberOfPoints)
            {
                positions.RemoveAt(0);
                line.SetPositions(positions.ToArray()); 
            }
        }
	}
}
