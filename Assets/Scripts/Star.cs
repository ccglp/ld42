﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

public class Star : MonoBehaviour {
    private SpriteRenderer rend;


    
    [SerializeField]
    private float alphaMin = 0.2f, alphaMax = 0.4f, timeToAnimate = 0.2f; 
	// Use this for initialization
	void Start () {
        rend = this.GetComponent<SpriteRenderer>();
        timeToAnimate = Random.Range(timeToAnimate - 0.1f, timeToAnimate + 0.1f);
        rend.color = new Color(1, 1, 1, alphaMin);
        FadeToMax(); 
	}

    private void FadeToMax()
    {
        rend.DOColor(new Color(1, 1, 1, alphaMax), timeToAnimate * 0.5f).OnComplete(()=>
        {
            FadeToMin(); 
        }); 
    }

    private void FadeToMin()
    {
        rend.DOColor(new Color(1, 1, 1, alphaMin), timeToAnimate * 0.5f).OnComplete(() =>
           {
               FadeToMax(); 
           });
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
