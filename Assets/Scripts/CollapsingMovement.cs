﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollapsingMovement : MonoBehaviour {
    private Vector3 direction;
    [SerializeField]
    private float speedBySecond = 2; 
     
	// Use this for initialization
	void Start () {
        direction = (GameObject.FindGameObjectWithTag("Center").transform.position - this.transform.position).normalized; 
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position += direction * speedBySecond * Time.deltaTime; 
	}
}
