﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement; 

public class MenuController : MonoBehaviour {

	[SerializeField]
	private GameObject text;

    private int mod = 1;

    [SerializeField]
    private float modSize = 0.1f;

    [SerializeField]
    private float animSpeed = 0.7f; 


	// Use this for initialization
	void Start () {
        Scale(); 

	}

    private void Scale(){
        text.transform.DOScale(1 + modSize * mod, animSpeed * 0.5f).OnComplete(()=>{
            mod *= -1;
            Scale(); 
        });
    }

	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown){
            SceneManager.LoadScene(1);
        }
	}


   
}
