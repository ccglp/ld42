﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    private SpriteRenderer rend; 

	// Use this for initialization
	void Start () {
        rend = this.GetComponentInChildren<SpriteRenderer>();
        rend.color = new Color(1, 1, 1, 0); 
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            rend.color = new Color(1, 1, 1, 0); 
        }
        else
        {
            rend.color = new Color(1, 1, 1, 1);
            Vector3 diff = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }

       
    }
}
