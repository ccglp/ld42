﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement; 

public class GameController : Singleton<GameController> {
    [SerializeField]
    private float timeToWin = 20;
    [SerializeField]
    private Image fillImage;

    [SerializeField]
    private GameObject nave;
    [SerializeField]
    private CanvasGroup cgWin,cgLose; 
    private float timer = 0;
    private bool gameEnded = false;
    private static int level = 1;

    [Header("Comet")]
    [SerializeField]
    private GameObject cometPrefab;
    [SerializeField]
    private Vector2 limitMin;
    [SerializeField]
    private AnimationCurve timeToSpawn; 
    [SerializeField]
    private bool cometsSpawn = true;
    private float timerComets = 0;
    private float timeTotalComets = 0; 

	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        fillImage.fillAmount = timer / timeToWin; 

        if (timer > timeToWin)
        {
            Win(); 
        }

        

        if (cometsSpawn){
            CometSpawn();
        }

        if (gameEnded && Input.GetKeyDown(KeyCode.Space))
        {
            ChargeLevel();  
        }
	}


    private void CometSpawn(){
		timerComets += Time.deltaTime;
        timeTotalComets += Time.deltaTime; 
        if (timerComets > timeToSpawn.Evaluate(timeTotalComets)){
            timerComets = 0;
            float x = Random.Range(limitMin.x - 2, limitMin.x);
            float y = Random.Range(limitMin.y - 2, limitMin.y);
            x = Random.Range(0, 100) > 50 ? x : -x;
            y = Random.Range(0, 100) > 50 ? y : -y;

            if (Random.Range(0,100) > 50){
                x = Random.Range(-limitMin.x, limitMin.x);
            }
            else{
                y = Random.Range(-limitMin.y, limitMin.y); 
            }
            Instantiate(cometPrefab, new Vector3(x,y,0), Quaternion.identity);
        }
	}
    public void Lose()
    {
        
        if (!gameEnded)
        {
            cgLose.DOFade(1, 0.3f);
            cgLose.interactable = true;
            cgLose.blocksRaycasts = true;
            gameEnded = true; 
            
        }
    }

    public void ChargeLevel()
    {
        SceneManager.LoadScene(level); 
    }
    private void Win()
    {
        if (!gameEnded)
        {
            level++; 
            gameEnded = true; 
            var particles = nave.GetComponentInChildren<ParticleSystem>();

            particles.Play(true);
            Debug.Log(particles);
            nave.transform.parent = null;
            DOVirtual.DelayedCall(2, () =>
            {
                cgWin.DOFade(1, 0.3f);
                cgWin.interactable = true;
                cgWin.blocksRaycasts = true;
            }).OnUpdate(() =>
            {
                nave.transform.position += Vector3.up * 1.2f * Time.deltaTime + Vector3.right * Time.deltaTime;
            });
        }
    }
}
